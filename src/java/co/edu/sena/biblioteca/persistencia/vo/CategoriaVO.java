/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.vo;

/**
 *
 * @author Carlos
 */
public class CategoriaVO {

    private Integer idCategoria;
    private String nombreCategoria;

    /**
     *
     * @param idCategoria
     * @param nombreCategoria
     */
    public CategoriaVO(Integer idCategoria, String nombreCategoria) {
        this.idCategoria = idCategoria;
        this.nombreCategoria = nombreCategoria;
    }

    /**
     *
     * @param idCategoria
     */
    public CategoriaVO(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    /**
     *
     */
    public CategoriaVO() {
    }

    /**
     *
     * @return
     */
    public Integer getIdCategoria() {
        return idCategoria;
    }

    /**
     *
     * @param idCategoria
     */
    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    /**
     *
     * @return
     */
    public String getNombreCategoria() {
        return nombreCategoria;
    }

    /**
     *
     * @param nombreCategoria
     */
    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

}
