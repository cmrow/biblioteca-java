/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.vo;

/**
 *
 * @author Carlos
 */
public class LibroVO {

    private Integer idLibro;
    private String isbn;
    private String tituloLibro;
    private String anioLibro;
    private String tocLibro;
    private Integer idAutor;
    private Integer idEditorial;
    private Integer idFormato;
    private Integer idCategoria;

    /**
     *
     * @param idLibro
     * @param isbn
     * @param tituloLibro
     * @param anioLibro
     * @param tocLibro
     * @param idAutor
     * @param idEditorial
     * @param idFormato
     * @param idCategoria
     */
    public LibroVO(Integer idLibro, String isbn, String tituloLibro, String anioLibro, String tocLibro, Integer idAutor, Integer idEditorial, Integer idFormato, Integer idCategoria) {
        this.idLibro = idLibro;
        this.isbn = isbn;
        this.tituloLibro = tituloLibro;
        this.anioLibro = anioLibro;
        this.tocLibro = tocLibro;
        this.idAutor = idAutor;
        this.idEditorial = idEditorial;
        this.idFormato = idFormato;
        this.idCategoria = idCategoria;
    }

    /**
     *
     */
    public LibroVO() {
    }

    /**
     *
     * @param idLibro
     */
    public LibroVO(Integer idLibro) {
        this.idLibro = idLibro;
    }

    /**
     *
     * @return
     */
    public Integer getIdLibro() {
        return idLibro;
    }

    /**
     *
     * @param idLibro
     */
    public void setIdLibro(Integer idLibro) {
        this.idLibro = idLibro;
    }

    /**
     *
     * @return
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     *
     * @param isbn
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     *
     * @return
     */
    public String getTituloLibro() {
        return tituloLibro;
    }

    /**
     *
     * @param tituloLibro
     */
    public void setTituloLibro(String tituloLibro) {
        this.tituloLibro = tituloLibro;
    }

    /**
     *
     * @return
     */
    public String getAnioLibro() {
        return anioLibro;
    }

    /**
     *
     * @param anioLibro
     */
    public void setAnioLibro(String anioLibro) {
        this.anioLibro = anioLibro;
    }

    /**
     *
     * @return
     */
    public String getTocLibro() {
        return tocLibro;
    }

    /**
     *
     * @param tocLibro
     */
    public void setTocLibro(String tocLibro) {
        this.tocLibro = tocLibro;
    }

    /**
     *
     * @return
     */
    public Integer getIdAutor() {
        return idAutor;
    }

    /**
     *
     * @param idAutor
     */
    public void setIdAutor(Integer idAutor) {
        this.idAutor = idAutor;
    }

    /**
     *
     * @return
     */
    public Integer getIdEditorial() {
        return idEditorial;
    }

    /**
     *
     * @param idEditorial
     */
    public void setIdEditorial(Integer idEditorial) {
        this.idEditorial = idEditorial;
    }

    /**
     *
     * @return
     */
    public Integer getIdFormato() {
        return idFormato;
    }

    /**
     *
     * @param idFormato
     */
    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    /**
     *
     * @return
     */
    public Integer getIdCategoria() {
        return idCategoria;
    }

    /**
     *
     * @param idCategoria
     */
    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }
}
