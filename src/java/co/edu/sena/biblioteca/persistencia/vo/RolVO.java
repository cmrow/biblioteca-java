/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.vo;

/**
 *
 * @author Carlos
 */
public class RolVO {

    private Integer idRol;
    private String nombreRol;

    /**
     *
     */
    public RolVO() {
    }

    /**
     *
     * @param idRol
     * @param nombreRol
     */
    public RolVO(Integer idRol, String nombreRol) {
        this.idRol = idRol;
        this.nombreRol = nombreRol;
    }

    public RolVO(int aInt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    public Integer getIdRol() {
        return idRol;
    }

    /**
     *
     * @param idRol
     */
    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    /**
     *
     * @return
     */
    public String getNombreRol() {
        return nombreRol;
    }

    /**
     *
     * @param nombreRol
     */
    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    public void setNombreCategoria(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
