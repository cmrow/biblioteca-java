/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.vo;

/**
 *
 * @author Carlos
 */
public class AutorVO {

    private Integer idAutor;
    private String nombreAutor;

    /**
     *
     */
    public AutorVO() {
    }

    /**
     *
     * @param idAutor
     * @param nombreAutor
     */
    public AutorVO(Integer idAutor, String nombreAutor) {
        this.idAutor = idAutor;
        this.nombreAutor = nombreAutor;
    }
/**
 * 
 * @param idAutor 
 */
    public AutorVO(Integer idAutor) {
        this.idAutor = idAutor;
    }
    /**
     *
     * @return
     */
    public Integer getIdAutor() {
        return idAutor;
    }

    /**
     *
     * @param idAutor
     */
    public void setIdAutor(Integer idAutor) {
        this.idAutor = idAutor;
    }

    /**
     *
     * @return
     */
    public String getNombreAutor() {
        return nombreAutor;
    }

    /**
     *
     * @param nombreAutor
     */
    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }

}
