/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.vo;

/**
 *
 * @author Carlos
 */
public class FormatoVO {

    private Integer idFormato;
    private String nombreFormato;

    /**
     *
     * @param idFormato
     * @param nombreFormato
     */
    public FormatoVO(Integer idFormato, String nombreFormato) {
        this.idFormato = idFormato;
        this.nombreFormato = nombreFormato;
    }

    /**
     *
     */
    public FormatoVO() {
    }

    public FormatoVO(int aInt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    public Integer getIdFormato() {
        return idFormato;
    }

    /**
     *
     * @param idFormato
     */
    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    /**
     *
     * @return
     */
    public String getNombreFormato() {
        return nombreFormato;
    }

    /**
     *
     * @param nombreFormato
     */
    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public void setNombreCategoria(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
