/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.vo;

/**
 *
 * @author Carlos
 */
public class VideoTutorialesVO {

    private Integer idVideoTutoriales;
    private String tituloVideoTutorial;
    private String ejercicioAdjunto;
    private String palabraClave;
    private Integer idAutor;

    public VideoTutorialesVO(Integer idVideoTutoriales) {
        this.idVideoTutoriales = idVideoTutoriales;
    }

    
    private Integer idProductora;
    private Integer idFormato;
    private Integer idCategoria;

    /**
     *
     * @param idVideoTutoriales
     * @param tituloVideoTutorial
     * @param ejercicioAdjunto
     * @param palabraClave
     * @param idAutor
     * @param idProductora
     * @param idFormato
     * @param idCategoria
     */
    public VideoTutorialesVO(Integer idVideoTutoriales, String tituloVideoTutorial, String ejercicioAdjunto, String palabraClave, Integer idAutor, Integer idProductora, Integer idFormato, Integer idCategoria) {
        this.idVideoTutoriales = idVideoTutoriales;
        this.tituloVideoTutorial = tituloVideoTutorial;
        this.ejercicioAdjunto = ejercicioAdjunto;
        this.palabraClave = palabraClave;
        this.idAutor = idAutor;
        this.idProductora = idProductora;
        this.idFormato = idFormato;
        this.idCategoria = idCategoria;
    }

    /**
     *
     */
    public VideoTutorialesVO() {
    }

  

    /**
     *
     * @return
     */
    public Integer getIdVideoTutoriales() {
        return idVideoTutoriales;
    }

    /**
     *
     * @param idVideoTutoriales
     */
    public void setIdVideoTutoriales(Integer idVideoTutoriales) {
        this.idVideoTutoriales = idVideoTutoriales;
    }

    /**
     *
     * @return
     */
    public String getTituloVideoTutorial() {
        return tituloVideoTutorial;
    }

    /**
     *
     * @param tituloVideoTutorial
     */
    public void setTituloVideoTutorial(String tituloVideoTutorial) {
        this.tituloVideoTutorial = tituloVideoTutorial;
    }

    /**
     *
     * @return
     */
    public String getEjercicioAdjunto() {
        return ejercicioAdjunto;
    }

    /**
     *
     * @param ejercicioAdjunto
     */
    public void setEjercicioAdjunto(String ejercicioAdjunto) {
        this.ejercicioAdjunto = ejercicioAdjunto;
    }

    /**
     *
     * @return
     */
    public String getPalabraClave() {
        return palabraClave;
    }

    /**
     *
     * @param palabraClave
     */
    public void setPalabraClave(String palabraClave) {
        this.palabraClave = palabraClave;
    }

    /**
     *
     * @return
     */
    public Integer getIdAutor() {
        return idAutor;
    }

    /**
     *
     * @param idAutor
     */
    public void setIdAutor(Integer idAutor) {
        this.idAutor = idAutor;
    }

    /**
     *
     * @return
     */
    public Integer getIdProductora() {
        return idProductora;
    }

    /**
     *
     * @param idProductora
     */
    public void setIdProductora(Integer idProductora) {
        this.idProductora = idProductora;
    }

    /**
     *
     * @return
     */
    public Integer getIdFormato() {
        return idFormato;
    }

    /**
     *
     * @param idFormato
     */
    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    /**
     *
     * @return
     */
    public Integer getIdCategoria() {
        return idCategoria;
    }

    /**
     *
     * @param idCategoria
     */
    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

}
