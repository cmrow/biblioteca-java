/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.vo;

/**
 *
 * @author Carlos
 */
public class EditorialesProductorasVO {

    private Integer idEditorialProductora;
    private String nombreEditorialProductora;

    /**
     *
     * @param idEditorialProductora
     * @param nombreEditorialProductora
     */
    public EditorialesProductorasVO(Integer idEditorialProductora, String nombreEditorialProductora) {
        this.idEditorialProductora = idEditorialProductora;
        this.nombreEditorialProductora = nombreEditorialProductora;
    }

    /**
     *
     * @param idEditorialProductora
     */
    public EditorialesProductorasVO(Integer idEditorialProductora) {
        this.idEditorialProductora = idEditorialProductora;
    }

    /**
     *
     */
    public EditorialesProductorasVO() {
    }

    /**
     *
     * @return
     */
    public Integer getIdEditorialProductora() {
        return idEditorialProductora;
    }

    /**
     *
     * @param idEditorialProductora
     */
    public void setIdEditorialProductora(Integer idEditorialProductora) {
        this.idEditorialProductora = idEditorialProductora;
    }

    /**
     *
     * @return
     */
    public String getNombreEditorialProductora() {
        return nombreEditorialProductora;
    }

    /**
     *
     * @param nombreEditorialProductora
     */
    public void setNombreEditorialProductora(String nombreEditorialProductora) {
        this.nombreEditorialProductora = nombreEditorialProductora;
    }

}
