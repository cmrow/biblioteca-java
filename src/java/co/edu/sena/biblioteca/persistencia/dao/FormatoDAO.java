/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.dao;

import co.edu.sena.biblioteca.persistencia.conexion.Conexion;
import co.edu.sena.biblioteca.persistencia.vo.FormatoVO;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.*;
/**
 *
 * @author Carlos
 */
public class FormatoDAO {
    
     public static List<FormatoVO> consultar() throws Exception {
        List<FormatoVO> lista = new ArrayList<>();
        try {
            CallableStatement cs = Conexion.conectar().prepareCall("{call pa_coansultar_formatos}");
            ResultSet rs = cs.executeQuery();
            while (rs.next()) {
                FormatoVO formato = new FormatoVO(rs.getInt("idFormato"));
                formato.setNombreCategoria(rs.getString("nombreFormato"));
                lista.add(formato);
            }
            Conexion.cerrar(cs, rs);
            return lista;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error de conexión");
        }
        
    }

    
}
