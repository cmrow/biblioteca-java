/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.dao;

import co.edu.sena.biblioteca.persistencia.conexion.Conexion;
import co.edu.sena.biblioteca.persistencia.vo.AutorVO;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.*;

/**
 *
 * @author Carlos
 */
public class AutorDAO {


    public static List<AutorVO> consultar() throws Exception {
        
        try {
            CallableStatement cs = null;
            ResultSet rs = null;
            List<AutorVO> lista = new ArrayList<>();
            cs = Conexion.conectar().prepareCall("{call pa_consultar_autores}");
            rs = cs.executeQuery();
            while (rs.next()) {
                AutorVO autor = new AutorVO(rs.getInt("idAutor"));
                autor.setNombreAutor(rs.getString("nombreAutor"));
                lista.add(autor);
            }
            Conexion.cerrar(cs,rs);
            return lista;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al conectar a la bd");
        }
    }
}
