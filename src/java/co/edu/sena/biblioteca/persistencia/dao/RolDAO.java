/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.dao;

import co.edu.sena.biblioteca.persistencia.conexion.Conexion;
import co.edu.sena.biblioteca.persistencia.vo.RolVO;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.*;
/**
 *
 * @author Carlos
 */
public class RolDAO {
        public static List<RolVO> consultar() throws Exception {
        List<RolVO> lista = new ArrayList<>();
        try {
            CallableStatement cs = Conexion.conectar().prepareCall("{call pa_coansultar_roles}");
            ResultSet rs = cs.executeQuery();
            while (rs.next()) {
                RolVO rolvo = new RolVO(rs.getInt("idRol"));
                rolvo.setNombreCategoria(rs.getString("nombreRol"));
                lista.add(rolvo);
            }
            Conexion.cerrar(cs, rs);
            return lista;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error de conexión");
        }

    }
    
}
