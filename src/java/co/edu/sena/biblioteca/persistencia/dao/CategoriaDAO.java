/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.dao;

import co.edu.sena.biblioteca.persistencia.conexion.Conexion;
import co.edu.sena.biblioteca.persistencia.vo.CategoriaVO;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.*;

/**
 *
 * @author Carlos
 */
public class CategoriaDAO {

    public static List<CategoriaVO> consultar() throws Exception {
        List<CategoriaVO> lista = new ArrayList<>();
        try {
            CallableStatement cs = Conexion.conectar().prepareCall("{call pa_consultar_categorias}");
            ResultSet rs = cs.executeQuery();
            while (rs.next()) {
                CategoriaVO categoria = new CategoriaVO(rs.getInt("idCategoria"));
                categoria.setNombreCategoria(rs.getString("nombreCategoria"));
                lista.add(categoria);
            }
            Conexion.cerrar(cs, rs);
            return lista;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error de conexión");
        }

    }
}
