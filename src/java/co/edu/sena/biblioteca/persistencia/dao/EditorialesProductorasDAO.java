/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.dao;

import co.edu.sena.biblioteca.persistencia.conexion.Conexion;
import co.edu.sena.biblioteca.persistencia.vo.EditorialesProductorasVO;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.*;

/**
 *
 * @author Carlos
 */
public class EditorialesProductorasDAO {

    public static List<EditorialesProductorasVO> consultar() throws Exception {
        List<EditorialesProductorasVO> lista = new ArrayList<>();
        try {
            CallableStatement cs = Conexion.conectar().prepareCall("{call pa_coansultar_editorialesproductoras}");
            ResultSet rs = cs.executeQuery();
            while (rs.next()) {
                EditorialesProductorasVO editorialProductora = new EditorialesProductorasVO(rs.getInt("idEditorialProductora"));
                editorialProductora.setNombreEditorialProductora(rs.getString("nombreEditorialProductora"));
                lista.add(editorialProductora);
            }
            Conexion.cerrar(cs, rs);
            return lista;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error de conexión");
        }

    }

}
