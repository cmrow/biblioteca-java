/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.dao;

import co.edu.sena.biblioteca.persistencia.conexion.Conexion;
import co.edu.sena.biblioteca.persistencia.vo.VideoTutorialesVO;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.*;
/**
 *
 * @author Carlos
 */
public class VideoTutorialesDAO {
        public static List<VideoTutorialesVO> consultar() throws Exception {
        List<VideoTutorialesVO> lista = new ArrayList<>();
        try {
            CallableStatement cs = Conexion.conectar().prepareCall("{call pa_coansultar_categorias}");
            ResultSet rs = cs.executeQuery();
            while (rs.next()) {
                VideoTutorialesVO videoTutorial = new VideoTutorialesVO(rs.getInt("idVideoTutorial"));
                videoTutorial.setTituloVideoTutorial(rs.getString("tituloVideoTutorial"));
                videoTutorial.setEjercicioAdjunto(rs.getString("ejercicioAdjunto"));
                videoTutorial.setPalabraClave(rs.getString("palabraClave"));
                videoTutorial.setIdAutor(rs.getInt("idAutor"));
                lista.add(videoTutorial);
            }
            Conexion.cerrar(cs, rs);
            return lista;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error de conexión");
        }

    }
    
}
