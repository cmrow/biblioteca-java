/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.dao;

import co.edu.sena.biblioteca.persistencia.conexion.Conexion;
import co.edu.sena.biblioteca.persistencia.vo.UsuarioVO;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.*;
/**
 *
 * @author Carlos
 */
public class UsuarioDAO {
        public static List<UsuarioVO> consultar() throws Exception {
        List<UsuarioVO> lista = new ArrayList<>();
        try {
            CallableStatement cs = Conexion.conectar().prepareCall("{call pa_coansultar_usuarios}");
            ResultSet rs = cs.executeQuery();
            while (rs.next()) {
                UsuarioVO usuario = new UsuarioVO(rs.getInt("idUsuario"));
                usuario.setNombreUsuario(rs.getString("nombreNombre"));
                usuario.setApellidoUsuario(rs.getString("apellidoUsuario"));
                usuario.setSeudonimo(rs.getString("seudonumo"));
                usuario.setClaveUsuario(rs.getString("claveUsuario"));
                usuario.setCorreoUsuario(rs.getString("correoUsuario"));
                usuario.setIdRolUsuario(rs.getInt("idRolUsuario"));
                lista.add(usuario);
            }
            Conexion.cerrar(cs, rs);
            return lista;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error de conexión");
        }

    }

    
}
