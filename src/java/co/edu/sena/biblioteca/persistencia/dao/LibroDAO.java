/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.biblioteca.persistencia.dao;

import co.edu.sena.biblioteca.persistencia.conexion.Conexion;
import co.edu.sena.biblioteca.persistencia.vo.LibroVO;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.*;

/**
 *
 * @author Carlos
 */
public class LibroDAO {
        public static List<LibroVO> consultar() throws Exception {
        List<LibroVO> lista = new ArrayList<>();
        try {
            CallableStatement cs = Conexion.conectar().prepareCall("{call pa_coansultar_libros}");
            ResultSet rs = cs.executeQuery();
            while (rs.next()) {
                LibroVO libro = new LibroVO(rs.getInt("idLibro"));
                libro.setIsbn(rs.getString("isbn"));
                libro.setTituloLibro(rs.getString("tituloLibro"));
                libro.setAnioLibro(rs.getString("anioLibro"));
                libro.setTocLibro(rs.getString("tocLibro"));
                libro.setIdAutor(rs.getInt("idAutor"));
                libro.setIdEditorial(rs.getInt("idEditorial"));
                libro.setIdFormato(rs.getInt("idFormato"));
                libro.setIdCategoria(rs.getInt("idCategoria"));
                lista.add(libro);
            }
            Conexion.cerrar(cs, rs);
            return lista;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error de conexión");
        }

    }
    
}
